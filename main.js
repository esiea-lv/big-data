import * as fs from "node:fs";
import * as readline from "node:readline";
import pm2 from "pm2";
import {join} from "node:path";

const CHUNK_SIZE = 10000;
let notHeader = false;
let workersAlive = [];
let chunkCounter = 0;
let actualChunk = 0;

const start = Date.now();
// Verify existence of tmp folder.
const tmpFolder = createOrResetTmpFolder();
// Now we will read through the CSV File.
const readInterface = readline.createInterface({
    input: fs.createReadStream('../assets/StockEtablissement_utf8.csv'),
    console: false,
});
let writeStream = fs.createWriteStream(join(tmpFolder, "chunk_" + chunkCounter + ".csv"));
let lineNumber = 0;
readInterface.on('line', line => {
    // With header skipped the first time, we push lines until it makes the size of a CHUNK in our file
    if (notHeader) {
        lineNumber++;
        writeStream.write(line + '\n');
        if (lineNumber === CHUNK_SIZE) {
            writeStream.end();
            chunkCounter++;
            lineNumber = 0;
            writeStream = fs.createWriteStream(join(tmpFolder, "chunk_" + chunkCounter + ".csv"));
        }
    } else {
        notHeader = true;
    }
})
readInterface.on('close', () => {
    // On end of file push the last file
    writeStream.end();
    writeStream.close();
    // Start processing the chunks
    startWorkers();
});

function createOrResetTmpFolder() {
    const tmpDir = './tmp';
    if (fs.existsSync(tmpDir)) {
        const files = fs.readdirSync(tmpDir);
        for (const file of files) {
            fs.unlinkSync(join(tmpDir, file));
        }
    } else {
        fs.mkdirSync(tmpDir);
    }
    return tmpDir;
}

function startWorkers() {
    pm2.connect((err) => {
        handleError(err)
        // Create our workers
        pm2.start({
            script: 'worker.js',
            name: 'Worker to send data to mongo db',
            instances: "12",
            exec_mode: "cluster",
            autorestart: false,
        }, (err, workers) => setTimeout(() => {
            handleError(err)
            // Each worker will receive a chunk of data
            for (let i = 0; i < workers.length; i++) {
                waitAndSendMessageToWorker(workers[i])
            }
            pm2.launchBus((err, pm2_bus) => {
                handleError(err);
                pm2_bus.on('process:msg', (packet) => {
                    if (packet.data.success) {
                        sendNewMessageToWorker(packet.process.pm_id)
                    }
                })
            })
        }, 500)); // Worker take time to spawn
    });
}

function waitAndSendMessageToWorker(worker) {
    setTimeout(() => {
        if (worker.pm2_env.status === 'online') {
            console.log('Starting ' + worker.pm2_env.pm_id)
            sendNewMessageToWorker(worker.pm2_env.pm_id);
            workersAlive.push(worker.pm2_env.pm_id);
        } else {
            waitAndSendMessageToWorker(worker);
        }
    }, 1000);
}

function sendNewMessageToWorker(workerId) {
    if (actualChunk <= chunkCounter) {
        const packet = {
            topic: 'process:msg',
            data: {
                chunkCounter: actualChunk
            }
        };
        actualChunk++;
        pm2.sendDataToProcessId(workerId, packet, err => {
            handleError(err);
        });
    } else {
        pm2.stop(workerId, err => {
            handleError(err);
            const index = workersAlive.indexOf(workerId);
            if (index !== -1) {
                console.log("Stopping worker " + workerId + ".");
                workersAlive.splice(index, 1);
            }
            if (workersAlive.length === 0) {
                const end = Date.now();
                console.log('Program ended, execution time: ' + (end - start) + ' ms.');
                pm2.disconnect();
                process.exit(0);
            }
        });
    }
}

function handleError(err) {
    if (err) {
        throw err;
    }
}


