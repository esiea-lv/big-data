# Louis VIGNES "Sirene Invader"

## Introduction
Le programme "Sirene Invader" est conçu pour importer des données CSV contenant des informations sur les établissements (siret, nic, etc.) dans une base de données MongoDB.
Le processus de chargement est divisé en plusieurs étapes distinctes, telles que la division du fichier CSV en chunks, le traitement parallèle de ces chunks par des travailleurs et l'importation des données dans la base de données MongoDB.

## Architecture du programme
Le programme est divisé en plusieurs fichiers et dépendances :

- `main.js`: Le fichier principal qui coordonne le processus d'importation.
- `worker.js`: Le script exécuté par chaque travailleur qui traite un chunk de données CSV.
- `model.js`: Le modèle de données Mongoose pour la collection MongoDB.
- `ecosystem.config.cjs`: Fichier de configuration PM2 pour le déploiement et la gestion des processus.
- `package.json`: Fichier de configuration du projet.

## Étapes du processus

### 1. Division du fichier CSV en chunks
1. Le programme commence par se connecter à la base de données MongoDB et supprime toutes les données existantes dans la collection "Siren".
2. Ensuite, il vérifie l'existence du dossier temporaire `./tmp` et le crée s'il n'existe pas, ou supprime son contenu s'il existe déjà.
3. Le fichier CSV source est ouvert en lecture ligne par ligne. Les lignes sont écrites dans des fichiers temporaires, chaque fichier contenant un
nombre spécifié de lignes (défini par `CHUNK_SIZE`).
4. Une fois que le fichier CSV est entièrement lu, le programme passe à l'étape suivante.

### 2. Traitement parallèle des chunks
1. Lorsque tous les chunks sont prêts, le programme démarre plusieurs travailleurs en utilisant PM2. Chaque travailleur est chargé de traiter un chunk spécifique.
2. Chaque travailleur reçoit un identifiant unique et un chunk de données à traiter.
3. Les travailleurs lisent leur chunk de données CSV, convertissent les lignes en documents MongoDB, puis insèrent ces documents dans la base de données.
4. Une fois qu'un travailleur a terminé de traiter son chunk, il signale son succès au processus principal.
5. Le processus principal envoie un nouveau chunk à chaque travailleur jusqu'à ce que tous les chunks soient traités.
6. Une fois que tous les chunks ont été traités, le programme se termine.

## Fonctionnement du programme
- Le fichier `main.js` coordonne les différentes étapes du processus d'importation.
- Les chunks de données sont traités de manière parallèle pour accélérer le processus d'importation.
- PM2 est utilisé pour gérer les travailleurs, assurant une utilisation efficace des ressources système.
- Le modèle de données Mongoose définit la structure des documents MongoDB pour assurer la cohérence des données.

## Exécution du programme
- Déposez un fichier CSV "StockEtablissement_utf8.csv" dans un dossier assets à l'extérieur du projet
- Créez une base mongodb siren avec la collection Siren
- Pour exécuter le programme, utilisez la commande `npm run run`.
- Cette commande démarre le processus principal à l'aide de PM2, qui à son tour coordonne les travailleurs pour importer les données.

## Topologie
![Topology.drawio.png](Topology.drawio.png)

## UML 
![UML.drawio.png](UML.drawio.png)

## Benchmarking
Chunks size

773839 --> 150 000

652817 --> 100 000

607321 --> 75000

605995 --> 50000

594951 --> 25000

**580105 --> 10000**

588381 --> 5000

585598 --> 1000

---

