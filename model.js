import {Schema} from "mongoose";

export const SirenModel = new Schema({
    "siren": Number,
    "nic": Number,
    "siret": Number,
    "dateCreationEtablissement": Date,
    "dateDernierTraitementEtablissement": Date,
    "typeVoieEtablissement": String,
    "libelleVoieEtablissement": String,
    "codePostalEtablissement": String,
    "libelleCommuneEtablissement": String,
    "codeCommuneEtablissement": String,
    "dateDebut": Date,
    "etatAdministratifEtablissement": String
}, {
    writeConcern: {
        w: "majority",
        j: false
    }
})
