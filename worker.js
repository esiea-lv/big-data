import mongoose from "mongoose";
import {SirenModel} from "./model.js";
import readline from "node:readline";
import fs from "node:fs";

const options = {lean: true, limit: null, ordered: false, rawResult: true};
// Connect preliminary to database and define model
let Siren = undefined;

process.on('message', (packet) => {
    if (!Siren) {
        mongoose.connect('mongodb://127.0.0.1:27017/sirene').then(() => {
            Siren = mongoose.model('Siren', SirenModel, "Siren");
            executeProcess(packet);
        });
    } else {
        executeProcess(packet);
    }
});

process.on('exit', () => {
    mongoose.connection.close().then(() => console.log('Connection closed'));
})


function executeProcess(packet) {
    const results = [];
    const readInterface = readline.createInterface({
        input: fs.createReadStream('./tmp/chunk_' + packet.data.chunkCounter + '.csv'),
        console: false,
    });
    readInterface.on('line', line => {
        const values = line.split(',');
        if (values.length >= 46) {
            const obj = new Siren({
                "siren": values[0],
                "nic": values[1],
                "siret": values[2],
                "dateCreationEtablissement": values[4],
                "dateDernierTraitementEtablissement": values[8],
                "typeVoieEtablissement": values[16],
                "libelleVoieEtablissement": values[17],
                "codePostalEtablissement": values[18],
                "libelleCommuneEtablissement": values[19],
                "codeCommuneEtablissement": values[22],
                "dateDebut": values[44],
                "etatAdministratifEtablissement": values[45]
            });
            results.push(obj);
        }
    });
    readInterface.on('error', (err) => {throw err;});
    readInterface.on('close', async () => {
        Siren.insertMany(results, options).then(() => {
            process.send({
                type: "process:msg",
                data: {
                    success: true
                }
            });
        });
    });
}
