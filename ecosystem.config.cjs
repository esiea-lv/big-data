module.exports = {
  apps : [{
    name   : "Sirene Invader Louis VIGNES",
    script : "./main.js",
    stop_exit_codes: [0],
    autorestart: false,
    detached: true,
    watch: false
  }]
}
